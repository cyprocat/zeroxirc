import re
from flask import Flask, request, session, g, url_for, abort, render_template, flash, escape, redirect, Markup
from flask_sqlalchemy import SQLAlchemy

app = Flask(__name__)

app.config.update(dict(
    SQLALCHEMY_DATABASE_URI="sqlite:///zeroxirc.db",
    SQLALCHEMY_TRACK_MODIFICATIONS="false",
    SECRET_KEY="TESTING_KEY",
))

app.jinja_env.cache = {}

global db
db = SQLAlchemy(app)

import zeroxirc.models.user
import zeroxirc.models.post

db.create_all()

import zeroxirc.helpers.setup

import zeroxirc.views.index

import zeroxirc.views.post.create
import zeroxirc.views.post.delete
import zeroxirc.views.post.edit

import zeroxirc.views.profile.edit
import zeroxirc.views.profile.list
import zeroxirc.views.profile.view

import zeroxirc.views.auth.register
import zeroxirc.views.auth.logout
import zeroxirc.views.auth.login
import zeroxirc.views.auth.templating

from zeroxirc.models.user import User

def power_helper(power=""):
    if "uuid" in zeroxirc.session:
        user = User.query.filter_by(uuid=zeroxirc.session["uuid"]).first()
        if user.rank.check_power(power) or power == "":
            return True
        else:
            zeroxirc.flash("you are not authorized to perform the action: " + power + "." , 'error')
            return redirect(url_for("index"))
    else:
        zeroxirc.flash("you are not logged in.", "warning")
        return redirect(url_for("index"))
