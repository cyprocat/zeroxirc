import zeroxirc, arrow, uuid, furl
from sqlalchemy_utils import ArrowType, EmailType, PasswordType, UUIDType, URLType

class Rank(zeroxirc.db.Model):
    __tablename__ = "rank"
    rank_id = zeroxirc.db.Column(zeroxirc.db.Integer, primary_key=True)
    name = zeroxirc.db.Column(zeroxirc.db.String(20), unique=True)
    powers = zeroxirc.db.Column(zeroxirc.db.String)

    def __init__(self, name, powers):
        self.name = name
        self.powers = powers

    def check_power(self, power):
        zeroxirc.helpers.auth.logged_out()
        if self.powers == "all":
            return True
        else:
            power_list = self.powers.split(",")
            if power in power_list:
                return True
            else:
                return False

class User(zeroxirc.db.Model):
    __tablename__ = "user"
    uuid = zeroxirc.db.Column(UUIDType(binary=False), primary_key=True)
    irc_nick = zeroxirc.db.Column(zeroxirc.db.String(16), unique=True)
    full_name = zeroxirc.db.Column(zeroxirc.db.String(64))
    email = zeroxirc.db.Column(EmailType, unique=True)
    bio = zeroxirc.db.Column(zeroxirc.db.String(1024))
    pronouns = zeroxirc.db.Column(URLType)
    website = zeroxirc.db.Column(URLType)
    twitter = zeroxirc.db.Column(zeroxirc.db.String(24))
    creation = zeroxirc.db.Column(ArrowType)
    rank_id = zeroxirc.db.Column(zeroxirc.db.Integer, zeroxirc.db.ForeignKey('rank.rank_id'))
    rank = zeroxirc.db.relationship('Rank', backref=zeroxirc.db.backref('user', lazy='dynamic'))
    password = zeroxirc.db.Column(PasswordType(schemes=['pbkdf2_sha512']))

    def __init__(self, irc_nick, password, rank, email=""):
        self.uuid = uuid.uuid1()
        self.irc_nick = irc_nick
        self.email = email
        self.full_name = ""
        self.bio = ""
        self.twitter = ""
        self.website = ""
        self.pronouns = ""
        self.creation = arrow.utcnow()
        self.rank = rank
        self.set_password(password)

    def set_password(self, password):
        self.password = password

    def check_password(self, password):
        return self.password == password
