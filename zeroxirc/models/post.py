import zeroxirc, arrow
from sqlalchemy_utils import ArrowType, UUIDType

class Post(zeroxirc.db.Model):
    __tablename__ = "post"
    post_id = zeroxirc.db.Column(zeroxirc.db.Integer, primary_key=True)
    author_id = zeroxirc.db.Column(UUIDType(binary=False), zeroxirc.db.ForeignKey('user.uuid'))
    author = zeroxirc.db.relationship('User', backref=zeroxirc.db.backref('post', lazy='dynamic'))
    pub_date = zeroxirc.db.Column(ArrowType)
    title = zeroxirc.db.Column(zeroxirc.db.String(20))
    content = zeroxirc.db.Column(zeroxirc.db.String(1536))
    category_id = zeroxirc.db.Column(zeroxirc.db.Integer, zeroxirc.db.ForeignKey('category.category_id'))
    category = zeroxirc.db.relationship('Category', backref=zeroxirc.db.backref('post', lazy='dynamic'))

    def __init__(self, title, content, category, author):
        self.title = title
        self.content = content
        self.category = category
        self.author = author
        self.pub_date = arrow.utcnow()

class Category(zeroxirc.db.Model):
    __tablename__ = "category"
    category_id = zeroxirc.db.Column(zeroxirc.db.Integer, primary_key=True)
    name = zeroxirc.db.Column(zeroxirc.db.String(16), unique=True)

    def __init__(self, name):
        self.name = name
