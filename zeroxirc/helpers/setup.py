import zeroxirc

from zeroxirc.models.user import User, Rank

checks = {
    "ircop":not Rank.query.filter_by(name="IRC Operator").first(),
    "user":not Rank.query.filter_by(name="User").first(),
    "kat":not User.query.filter_by(irc_nick="kat").first()
}

if checks["ircop"]:
    admin_rank = Rank("IRC Operator", "all")
    zeroxirc.db.session.add(admin_rank)

if checks["user"]:
    user_rank = Rank("User", "")
    zeroxirc.db.session.add(user_rank)

if checks["kat"]:
    kat_user = User("kat", "loldongs1337", admin_rank, "kat@cyprocat.me")
    zeroxirc.db.session.add(kat_user)

zeroxirc.db.session.commit()
