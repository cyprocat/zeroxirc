import zeroxirc

def logged_out():
    if "uuid" in zeroxirc.session:
        return True
    else:
        zeroxirc.flash("you are not logged in.", "warning")
        return zeroxirc.redirect(zeroxirc.url_for("index"))

def logged_in():
    if "uuid" in zeroxirc.session:
        zeroxirc.flash("you are already logged in.", "warning")
        return zeroxirc.redirect(zeroxirc.url_for("index"))
    else:
        return False
