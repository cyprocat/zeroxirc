import zeroxirc

from zeroxirc.models.post import Post, Category

@zeroxirc.app.route("/edit_post/<post_id>")
def edit_post_form(post_id):
    zeroxirc.power_helper("edit_post")
    getPost = Post.query.filter_by(post_id=post_id).first()
    categories = Category.query.all().remove(getPost.category)
    return zeroxirc.render_template("post/edit.html.jinja2", categories=categories, post=getPost)

@zeroxirc.app.route("/functions/edit_post/<post_id>", methods=["POST", "GET"])
def edit_post(post_id):
    zeroxirc.power_helper("edit_post")
    post = Post.query.filter_by(post_id=post_id).first()

    formFields, fetched = ["title", "content", "categories"], {}

    newCategoryFlag = False
    for item in formFields:
        fetchedData = zeroxirc.request.form[item]
        if item == "categories" and fetchedData == "other":
            fetched[categories] = zeroxirc.request.form["new_category"]
            newCategoryFlag = True
        else:
            fetched[item] = fetchedData

    for attr, value in fetched.items():
        if attr is "categories":
            if newCategoryFlag:
                newCategory = Session(value)
                zeroxirc.db.session.add(newCategory)
                post.category = newCategory
        else:
            if getattr(post, attr) is not value:
                setattr(post, attr, value)

    zeroxirc.db.session.commit()
    zeroxirc.flash("the post has been successfully edited.", "success")
    return zeroxirc.redirect(zeroxirc.url_for("index"))
