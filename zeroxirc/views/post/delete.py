import zeroxirc

from zeroxirc.models.post import Post

@zeroxirc.app.route("/delete_post/<post_id>")
def delete_post_form(post_id):
    zeroxirc.power_helper("delete_post")
    post = Post.query.filter_by(post_id=post_id).first()
    msg = zeroxirc.Markup("are you sure you want to delete the post named \"%s\"? <div class=\"pull-right\"><a href=\"%s\">yes</a> <a href=\"%s\">no</a></div>" % (post.title, zeroxirc.url_for('delete_post', post_id=post_id), zeroxirc.url_for('index')))
    zeroxirc.flash(msg , 'warning')
    return zeroxirc.redirect(zeroxirc.url_for("index"))

@zeroxirc.app.route("/functions/delete_post/<post_id>")
def delete_post(post_id):
    zeroxirc.power_helper("delete_post")
    post = Post.query.filter_by(post_id=post_id).first()
    msg = "the post named \"%s\" has been deleted." % post.title
    zeroxirc.db.session.delete(post)
    zeroxirc.db.session.commit()
    zeroxirc.flash(msg , 'warning')
    return zeroxirc.redirect(zeroxirc.url_for("index"))
