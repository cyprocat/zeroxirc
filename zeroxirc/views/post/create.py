import zeroxirc

from zeroxirc.models.post import Post, Category

@zeroxirc.app.route("/create_post")
def create_post_form():
    zeroxirc.power_helper("create_post")
    categories = Category.query.all()
    return zeroxirc.render_template("post/make.html.jinja2", categories=categories)

@zeroxirc.app.route("/functions/create_post", methods=["POST", "GET"])
def create_post():
    zeroxirc.power_helper("create_post")
    formFields, fetched = ["title", "content", "categories"], {}

    newCategoryFlag = False
    for item in formFields:
        fetchedData = zeroxirc.request.form[item]
        if item == "categories" and fetchedData == "other":
            fetched["categories"] = zeroxirc.request.form["new_category"]
            newCategoryFlag = True
        else:
            fetched[item] = fetchedData

    user = User.query.filter_by(uuid=zeroxirc.session["uuid"]).first()

    if newCategoryFlag:
        newCategory = Category(fetched["categories"])
        newPost = Post(fetched["title"], fetched["content"], newCategory, user)
        zeroxirc.db.session.add(newCategory)
    else:
        existingCategory = Category.query.filter_by(name=fetched["categories"]).first()
        if not existingCategory:
            zeroxirc.flash("the category chosen is not within the database.", 'error')
            return zeroxirc.zeroxirc.redirect(zeroxirc.zeroxirc.url_for("make_post_form"))
        newPost = Post(fetched["title"], fetched["content"], existingCategory, user)

    zeroxirc.db.session.add(newPost)
    zeroxirc.db.session.commit()
    zeroxirc.flash("your post was successful.", 'success')
    return zeroxirc.zeroxirc.redirect(zeroxirc.zeroxirc.url_for("index"))
