import zeroxirc

from zeroxirc.models.user import User
from zeroxirc.models.post import Post, Category

@zeroxirc.app.route("/")
def index():
    posts = Post.query.all()[::-1]
    return zeroxirc.render_template("index.html.jinja2", posts=posts)
