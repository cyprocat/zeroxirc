import zeroxirc

from zeroxirc.models.user import User
from zeroxirc.helpers.auth import logged_out

@zeroxirc.app.route("/edit_profile")
def edit_profile_form():
    return zeroxirc.render_template("/profile/edit.html.jinja2")

@zeroxirc.app.route("/functions/edit_profile", methods=["POST", "GET"])
def edit_profile():
    fields = ["irc_nick", "full_name", "pronouns", "website", "twitter", "email", "bio"]
    form_info = {}
    user = User.query.filter_by(uuid=zeroxirc.session["uuid"]).first()

    for field in fields:
        form_info[field] = zeroxirc.request.form[field]

    for field, info in form_info.items():
        if field == "irc_nick" and info == "":
            zeroxirc.flash("you must have an irc nick.", "warning")
            return zeroxirc.redirect(zeroxirc.url_for("edit_profile_form"))
        if info is not getattr(user, field):
            setattr(user, field, info)

    zeroxirc.db.session.commit()
    zeroxirc.flash("your changes have been saved.", "success")
    return zeroxirc.redirect(zeroxirc.url_for("edit_profile_form"))
