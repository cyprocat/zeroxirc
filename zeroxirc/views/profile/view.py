import zeroxirc

from zeroxirc.models.user import User

@zeroxirc.app.route("/profile/<irc_nick>")
def user_profile(irc_nick):
    user = User.query.filter_by(irc_nick=irc_nick).first()
    return zeroxirc.render_template("profile/view.html.jinja2", their_user=user)
