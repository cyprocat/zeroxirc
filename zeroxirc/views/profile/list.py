import zeroxirc

from zeroxirc.models.user import User

@zeroxirc.app.route("/user_list")
def user_list():
    users = User.query.all()
    return zeroxirc.render_template("profile/list.html.jinja2", users=users)
