import zeroxirc

from zeroxirc.models.user import User

@zeroxirc.app.context_processor
def user_info():
    if "uuid" in zeroxirc.session:
        user = User.query.filter_by(uuid=zeroxirc.session["uuid"]).first()
        user_count = zeroxirc.db.session.query(User).count()
        if user:
            return {"user":user, "user_count":user_count}
    else:
        return {"user":None, "user_count":0}
