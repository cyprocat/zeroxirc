import zeroxirc

from zeroxirc.models.user import User
from zeroxirc.helpers.auth import logged_out

@zeroxirc.app.route("/logout/<uuid>")
def logout_form(uuid):
    logged_out()
    msg = zeroxirc.Markup("are you sure you want to logout? <div class=\"pull-right\"><a href=\"%s\">yes</a> <a href=\"%s\">no</a></div>" % (zeroxirc.url_for('logout', uuid=uuid), zeroxirc.url_for('index')))
    zeroxirc.flash(msg, 'warning')
    return zeroxirc.redirect(zeroxirc.url_for("index"))

@zeroxirc.app.route('/functions/logout/<uuid>')
def logout(uuid):
    logged_out()
    if str(zeroxirc.session["uuid"]) == uuid:
        zeroxirc.flash("you have been logged out.", 'success')
        zeroxirc.session.pop('uuid', None)
        return zeroxirc.redirect(zeroxirc.url_for('index'))
    else:
        zeroxirc.flash("invalid request.", 'error')
        return zeroxirc.redirect(zeroxirc.url_for('index'))
