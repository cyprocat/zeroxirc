import zeroxirc

from zeroxirc.models.user import User, Rank
from zeroxirc.helpers.auth import logged_in

@zeroxirc.app.route("/register")
def register_form():
    logged_in()
    return zeroxirc.render_template("auth/register.html.jinja2")

@zeroxirc.app.route("/functions/register", methods=["POST", "GET"])
def register():
    logged_in()
    if zeroxirc.request.method == "POST":
        fields = ["irc_nick", "email", "password"]
        form_info = {}

        for field in fields:
            form_info[field] = zeroxirc.request.form[field]

        errors = {
            "nick_exists":False,
            "missing":[],
            "email":False,
            "nick_regex":False
        }
        for field, content in form_info.items():
            if not content and field is not "email":
                errors["missing"].append(field.replace("_", " "))
            if field is "irc_nick" and content:
                if User.query.filter_by(irc_nick=content).first():
                    errors["nick_exists"] = True
                if not zeroxirc.re.search('^[a-z][a-z0-9-_]{2,32}$', content, zeroxirc.re.IGNORECASE):
                    errors["nick_regex"] = True
            if field is "email" and content:
                if not zeroxirc.re.match(r"[^@]+@[^@]+\.[^@]+", content):
                    errors["email"] = True

        if len(errors["missing"]) or errors["nick_exists"] or errors["email"] or errors["nick_regex"]:
            for error, content in errors.items():
                if error == "nick_exists":
                    msg = "the nickname \"%s\" is already taken." % form_info["irc_nick"]
                    zeroxirc.flash(msg, "warning")
                elif error == "email":
                    msg = "the email \"%s \"provided is not valid." % form_info["email"]
                    zeroxirc.flash(msg, "warning")
                elif error == "nick_regex":
                    msg = "the nickname \"%s\" is not valid as it does not meet irc nickname standards as set in the rfcs." % form_info["irc_nick"]
                    zeroxirc.flash(msg, "warning")
                elif error == "missing" and len(errors["missing"]):
                    if len(errors["missing"]) > 1:
                        msg = "you must enter both an IRC nickname and a password."
                    else:
                        def switch(error):
                            return {
                                "irc_nick":"you must enter an IRC nickname.",
                                "password":"you must enter a password."
                            }[error]
                        msg = switch(errors["missing"][0])
                    zeroxirc.flash(msg, "warning")
            return zeroxirc.redirect(zeroxirc.url_for("register_form"))

        if form_info["email"]:
            new_user = User(form_info["irc_nick"], form_info["password"], Rank.query.filter_by(name="User").first(), form_info["email"])
        else:
            new_user = User(form_info["irc_nick"], form_info["password"], Rank.query.filter_by(name="User").first())

        zeroxirc.db.session.add(new_user)
        zeroxirc.db.session.commit()
        zeroxirc.session["uuid"] = new_user.uuid
        zeroxirc.flash("your registration was successful!", "success")
        return zeroxirc.redirect(zeroxirc.url_for("index"))
