import zeroxirc

from zeroxirc.models.user import User
from zeroxirc.helpers.auth import logged_in

@zeroxirc.app.route("/functions/login", methods=["POST", "GET"])
def login():
    logged_in()

    if zeroxirc.request.method == "POST":
        irc_nick = zeroxirc.request.form["irc_nick"]
        password = zeroxirc.request.form["password"]

        user = User.query.filter_by(irc_nick=irc_nick).first()

        if user:
            if user.check_password(password):
                zeroxirc.session["uuid"] = user.uuid

                msg = "you have logged in as \"%s\"." % user.irc_nick
                zeroxirc.flash(msg, 'success')
                return zeroxirc.redirect(zeroxirc.url_for("index"))
            else:
                msg = "either the user does not exist or you have entered an incorrect password."
                zeroxirc.flash(msg, 'warning')
                return zeroxirc.redirect(zeroxirc.url_for("index"))
        else:
                msg = "either the user does not exist or you have entered an incorrect password."
                zeroxirc.flash(msg, 'warning')
                return zeroxirc.redirect(zeroxirc.url_for("index"))
