from setuptools import setup

setup(
    name="0xIRC",
    packages=["zeroxirc"],
    include_package_data=True,
    install_requires=[
        "flask",
        "flask_sqlalchemy",
        "sqlalchemy_utils",
        "arrow",
    ],
)
